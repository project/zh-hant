# Chinese, Traditional translation of drupal (6.2)
# Copyright (c) 2008 by the Chinese, Traditional translation team
# Generated from files:
#  trigger.admin.inc,v 1.5 2008/01/08 10:35:43 goba
#  trigger.module,v 1.13.2.1 2008/04/09 21:11:51 goba
#  trigger.install,v 1.5 2007/12/28 12:02:52 dries
#
msgid ""
msgstr ""
"Project-Id-Version: drupal (6.x)\n"
"POT-Creation-Date: 2008-05-14 13:25+0800\n"
"PO-Revision-Date: 2009-08-20 00:23+0800\n"
"Language-Team: Drupal Taiwan <info@drupaltaiwan.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Last-Translator: charlesc <charles@netivism.com.tw>\n"
"X-Poedit-Language: Chinese\n"
"X-Poedit-Country: TAIWAN\n"
"X-Poedit-SourceCharset: utf-8\n"
"Plural-Forms: nplurals=2; plural=1;\n"

#: modules/trigger/trigger.admin.inc:74
msgid "Are you sure you want to unassign the action %title?"
msgstr "你真的要取消指派%title動作嗎？"

#: modules/trigger/trigger.admin.inc:76
msgid "You can assign it again later if you wish."
msgstr "如果你想要的話，你可以再次指派它。"

#: modules/trigger/trigger.admin.inc:88,87
msgid "Action %action has been unassigned."
msgstr "已取消指派%action動作。"

#: modules/trigger/trigger.admin.inc:144
msgid "Trigger: "
msgstr "觸發器："

#: modules/trigger/trigger.admin.inc:154
msgid "unassign"
msgstr "取消指派"

#: modules/trigger/trigger.admin.inc:171
msgid "Assign"
msgstr "指派"

#: modules/trigger/trigger.admin.inc:176
msgid "No available actions for this trigger."
msgstr "此觸發器沒有可用的動作。"

#: modules/trigger/trigger.admin.inc:192
msgid "The action you chose is already assigned to that trigger."
msgstr "你所選擇的動作已經被指派給這觸發器了。"

#: modules/trigger/trigger.admin.inc:218
msgid ""
"You have added an action that changes a the property of a post. A Save "
"post action has been added so that the property change will be saved."
msgstr "你增加了一個可以改變文章屬性的動作。加入了一個儲存文章的動作，以儲存屬性的改變。"

#: modules/trigger/trigger.admin.inc:238
msgid "Operation"
msgstr "操作"

#: modules/trigger/trigger.module:14
msgid ""
"Triggers are system events, such as when new content is added or when "
"a user logs in. Trigger module combines these triggers with actions "
"(functional tasks), such as unpublishing content or e-mailing an "
"administrator. The <a href=\"@url\">Actions settings page</a> contains "
"a list of existing actions and provides the ability to create and "
"configure additional actions."
msgstr ""
"觸發器是系統事件，例如當加入了新內容或是使用者登入時。觸發器模組包含這些觸發器與動作，例如取消發表內容或是寄電子郵件給管理者。<a "
"href=\"@url\">動作設定頁</a>包含了許多現有動作的列表，並可以建立與設定額外的動作。"

#: modules/trigger/trigger.module:17
msgid ""
"Below you can assign actions to run when certain comment-related "
"triggers happen. For example, you could promote a post to the front "
"page when a comment is added."
msgstr "你可以在下面設定當某些回應相關的觸發器發動時，所要執行的動作。舉例來說，你可以在有回應加入時，將一篇文章推薦到首頁。"

#: modules/trigger/trigger.module:19
msgid ""
"Below you can assign actions to run when certain content-related "
"triggers happen. For example, you could send an e-mail to an "
"administrator when a post is created or updated."
msgstr "你可以在下面設定當某些內容相關的觸發器發動時，所要執行的動作。舉例來說，你可以在新增或更新文章時，寄送電子郵件給使用者。"

#: modules/trigger/trigger.module:21
msgid ""
"Below you can assign actions to run during each pass of a <a "
"href=\"@cron\">cron maintenance task</a>."
msgstr ""
"你可以在下面設定當 <a "
"href=\"@cron\">cron維護作業</a>時，所要執行的動作。"

#: modules/trigger/trigger.module:23
msgid ""
"Below you can assign actions to run when certain taxonomy-related "
"triggers happen. For example, you could send an e-mail to an "
"administrator when a term is deleted."
msgstr "你可以在下面設定當某些分類相關的觸發器發動時，所要執行的動作。舉例來說，你可以在刪除分類時寄送電子郵件給管理者。"

#: modules/trigger/trigger.module:25
msgid ""
"Below you can assign actions to run when certain user-related triggers "
"happen. For example, you could send an e-mail to an administrator when "
"a user account is deleted."
msgstr "你可以在下面設定當某些使用者相關的觸發器發動時，所要執行的動作。舉例來說，你可以在使用者帳號被刪除時，可以傳送電子郵件給使用者。"

#: modules/trigger/trigger.module:27
msgid ""
"The Trigger module provides the ability to trigger <a "
"href=\"@actions\">actions</a> upon system events, such as when new "
"content is added or when a user logs in."
msgstr ""
"觸發器模組提供了可以在有系統事件發生時，觸發<a "
"href=\"@actions\">動作</a>的功能。例如在有新文章發表時、或是使用者登入時。"

#: modules/trigger/trigger.module:28
msgid ""
"The combination of actions and triggers can perform many useful tasks, "
"such as e-mailing an administrator if a user account is deleted, or "
"automatically unpublishing comments that contain certain words. By "
"default, there are five \"contexts\" of events (Comments, Content, "
"Cron, Taxonomy, and Users), but more may be added by additional "
"modules."
msgstr ""
"動作與觸發器的組合，可以執行許多有用的工作，例如在使用者帳號被刪除時寄送電子郵件給使用者，或是在回應裡出現某些字詞時自動隱藏回應。根據預設值，有 "
"5 "
"種事件脈絡（回應、內容、cron、分類和使用者），但是其他模組還可以新增更多的事件。"

#: modules/trigger/trigger.module:29
msgid ""
"For more information, see the online handbook entry for <a "
"href=\"@trigger\">Trigger module</a>."
msgstr ""
"更多資訊，請參考<a "
"href=\"@trigger\">觸發器</a>的線上說明。"

#: modules/trigger/trigger.module:39
msgid "Triggers"
msgstr "觸發器"

#: modules/trigger/trigger.module:40
msgid "Tell Drupal when to execute actions."
msgstr "告訴 Drupal 要在何時執行動作。"

#: modules/trigger/trigger.module:85
msgid "Cron"
msgstr "cron"

#: modules/trigger/trigger.module:115
msgid "Unassign an action from a trigger."
msgstr "把某一動作從觸發器裡移除。"

#: modules/trigger/trigger.module:0
msgid "trigger"
msgstr "觸發器"

#: modules/trigger/trigger.install:28
msgid "Maps trigger to hook and operation assignments from trigger.module."
msgstr "把觸發器與觸發器模組的 hook 和操作任務進行對應。"

#: modules/trigger/trigger.install:35
msgid ""
"Primary Key: The name of the internal Drupal hook upon which an action "
"is firing; for example, nodeapi."
msgstr ""
"Primary Key: 這個 Drupal "
"內部名稱串連引發一個行動，例如：nodeapi。"

#: modules/trigger/trigger.install:42
msgid ""
"Primary Key: The specific operation of the hook upon which an action "
"is firing: for example, presave."
msgstr ""
"Primary Key: 這個動作串連引發一個行動，例如： "
"presave。"

#: modules/trigger/trigger.install:49
msgid "Primary Key: Action's {actions}.aid."
msgstr "Primary Key: Action's {actions}.aid."

#: modules/trigger/trigger.install:55
msgid "The weight of the trigger assignment in relation to other triggers."
msgstr "觸發器指派的順序值（相對於與其他觸發器）。"

