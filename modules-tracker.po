# Chinese, Traditional translation of drupal (6.2)
# Copyright (c) 2008 by the Chinese, Traditional translation team
# Generated from files:
#  tracker.module,v 1.154.2.1 2008/04/09 21:11:51 goba
#  tracker.pages.inc,v 1.5 2007/11/28 10:29:20 dries
#
msgid ""
msgstr ""
"Project-Id-Version: drupal (6.x)\n"
"POT-Creation-Date: 2008-05-14 13:25+0800\n"
"PO-Revision-Date: 2009-08-20 00:23+0800\n"
"Language-Team: Drupal Taiwan <info@drupaltaiwan.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Last-Translator: charlesc <charles@netivism.com.tw>\n"
"X-Poedit-Language: Chinese\n"
"X-Poedit-Country: TAIWAN\n"
"X-Poedit-SourceCharset: utf-8\n"
"Plural-Forms: nplurals=2; plural=1;\n"

#: modules/tracker/tracker.module:27
msgid "Recent posts"
msgstr "最新文章"

#: modules/tracker/tracker.pages.inc:57
msgid "!time ago"
msgstr "!time 以前"

#: modules/tracker/tracker.pages.inc:65
msgid "Post"
msgstr "發表"

#: modules/tracker/tracker.pages.inc:65
msgid "Last updated"
msgstr "最近更新"

#: modules/tracker/tracker.module:15
msgid ""
"The tracker module displays the most recently added or updated content "
"on your site, and provides user-level tracking to follow the "
"contributions of particular authors."
msgstr "tracker模組顯示出您的網站最近新加入或者更新的內容，並且提供一個使用者層級的追蹤機制，追蹤特定作者的貢獻文章。"

#: modules/tracker/tracker.module:16
msgid ""
"The <em>Recent posts</em> page is available via a link in the "
"navigation menu block and displays new and recently-updated content "
"(including the content type, the title, the author's name, number of "
"comments, and time of last update) in reverse chronological order. "
"Posts are marked updated when changes occur in the text, or when new "
"comments are added. To use the tracker module to follow a specific "
"user's contributions, select the <em>Track</em> tab from the user's "
"profile page."
msgstr ""
"導覽選單區塊中有個<em>最新文章</em>的連結連到該頁面，以相反的時間順序顯示新的與最近更新過的內容(包括內容類型、標題、作者姓名、回應數與最近更新時間)。當文章的文字內容有所變更或者被加入一篇回應時，文章就會被標記為「更新」。要使用tracker "
"模組來追蹤特定使用者的貢獻文章，請在該使用者的個人檔案頁面中選擇<em>追蹤</em>標籤。"

#: modules/tracker/tracker.module:17
msgid ""
"For more information, see the online handbook entry for <a "
"href=\"@tracker\">Tracker module</a>."
msgstr ""
"更多資訊，請見線上手冊項目：<a href=\"@tracker\">Tracker "
"模組</a>."

#: modules/tracker/tracker.module:34
msgid "All recent posts"
msgstr "所有最新文章"

#: modules/tracker/tracker.module:38
msgid "My recent posts"
msgstr "我的最新文章"

#: modules/tracker/tracker.module:55
msgid "Track posts"
msgstr "追蹤文章"

#: modules/tracker/tracker.module:0
msgid "tracker"
msgstr "追蹤"

